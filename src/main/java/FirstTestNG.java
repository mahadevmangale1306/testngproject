import org.testng.annotations.*;

public class FirstTestNG {
    @BeforeSuite
    public void first()
    {
        System.out.println("Inside Before Suit....");
    }
    @AfterSuite
    public void second()
    {
        System.out.println("Inside After Suit...");
    }

    @BeforeClass
    public void third()
    {
        System.out.println("Inside Before Class....");
    }
    @AfterClass
    public void forth()
    {
        System.out.println("Inside After Class...");
    }

    @BeforeTest
    public void five()
    {
        System.out.println("Inside Before Test....");
    }
    @AfterTest
    public void six()
    {
        System.out.println("Inside After Test...");
    }

    @BeforeMethod
    public void seven()
    {
        System.out.println("Inside Before Method....");
    }
    @AfterMethod
    public void eight()
    {
        System.out.println("Inside After Method...");
    }

    @Test
    public  void nine()
    {
        System.out.println("Inside test method..");
    }

    @BeforeGroups
    public void ten()
    {
        System.out.println("Inside Before groups....");
    }
    @AfterGroups
    public void eleven()
    {
        System.out.println("Inside After groups...");
    }

}

/*
    output:
Inside Before Suit....
Inside Before Test....
Inside Before Class....
Inside Before Method....
Inside test method..
Inside After Method...
Inside After Class...
Inside After Test...
Inside After Suit...

*/
